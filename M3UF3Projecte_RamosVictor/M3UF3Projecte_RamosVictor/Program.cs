﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M3UF3Projecte_RamosVictor
{
    class Program
    {
        static void Main(string[] args)
        {
             Program p = new Program();
             p.Inici();
        }
        public void Inici()
        {
            bool fin;
            do
            {
                Console.Clear();
                MostrarMenu();
                string opcion = Console.ReadLine();
                fin = CallExercice(opcion);
            } while (!fin);

        }
        //Menu de inicio
        void MostrarMenu()
        {
            Console.WriteLine("[1]: Instrucciones");
            Console.WriteLine("[2]: Leer Ranking");
            Console.WriteLine("[3]: Wordle");
            Console.WriteLine("[0]: Sortir");
        }

        //Menu de inicio
        bool CallExercice(string opcion)
        {
            Console.Clear();
            Wordle wordgame = new Wordle();
            switch (opcion)
            {
                case "1":
                    Instrucciones();
                    break;
                case "2":
                    LeerRanking();
                    break;
                case "3":
                    wordgame.Idioma();
                    break;
                
                case "0":
                    return true;

                default:
                    Console.WriteLine("Esta opció no existeix");
                    break;
            }

            return false;
        }
        void LeerRanking()
        {
            string path = @"..\..\..\Ranking.txt";
            string text = File.ReadAllText(path);
            Console.WriteLine(text);
            Console.WriteLine("Presione cualquier tecla para continuar.");
            Console.ReadKey();
        }


        //Programa que da las instrucciones del juego
        void Instrucciones()
        {
            Console.WriteLine("Para jugar a este juego deeras introducir palabras con 5 letras del idioma escogido");
            Console.WriteLine("Una vez dentro del juego, el jugador tendra 6 turnos para adivinar la palabra");
            Console.WriteLine("Para ello el programa dara diversas pistas");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("La letra saldra verde si esta en el lugar correcto");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("La letra saldra amarillo si la palabra contiene la letra pero esta mal situada");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Y la letra saldra roja si la palabra no contiene esa letra");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("En caso de ganar, el programa pedira el nombre al jugador para almacenar la puntuacion");
            Console.ResetColor();
            Console.WriteLine("Para salir del juego escriba [END]");
            Console.ReadLine();

        }

    }

    class Wordle
    {
        private int intentos;
        public string playerName;


        public void Idioma()
        {

            bool fin;
            do
            {
                Console.Clear();
                MenuIdioma();
                string opcion = Console.ReadLine();
                fin = IdiomaWordle(opcion);
            } while (!fin);
        }
        public void MenuIdioma()
        {
            Console.WriteLine("Elige el idioma que quiera para jugar");
            Console.WriteLine("[esp]: En español");
            Console.WriteLine("[cat]: En catalan");           
            Console.WriteLine("[0]: Salir");
        }
        //Menu donde el jugador seleccionara el idioma en el que quiere jugar
        bool IdiomaWordle(string opcion)
        {
            Console.Clear();
            switch (opcion)
            {
                case "esp":
                    string[] palabrases = { "abuso", "guapo", "nubla", "limbo", "perro", "flaco", "calvo", "cajas",
                  "cebra", "sable", "quema", "llama", "bello", "ojear", "ciego", "asilo", "venir", "dosis"
                , "borde", "fruta", "barco", "grano", "enano", "hacha", "huevo"
                , "lapso", "menta", "nadie", "obeso", "poder", "pilar", "razon"
                , "saber", "valor", "dulce", "claro", "breve", "corte", "chico"};
                    WordleGame(palabrases);
                    break;
                case "cat":
                    string[] palabrascat = { "porus", "amics", "llavi", "rabia", "ullal", "oblid", "escut", "magra",
                  "claus", "tronc", "cursa", "omple", "agafa", "malva", "ronda", "haver", "valor", "taula"
                , "lluna", "marxa", "aigua", "corba", "capsa", "terra", "sabat"
                , "esfer", "flama", "poble", "estiu", "vinya", "sucre", "sangs"
                , "dansa", "cadet", "pilat", "greix", "pruna", "remei", "senta"};
                    WordleGame(palabrascat);
                    break;

             
                case "0":
                   
                    return true;

                default:
                    Console.WriteLine("Esta opció no existeix");
                    break;
            }

            return false;
        }

        //Programa principal desde donde se ejecuta el programa
        public void WordleGame(string[] palabras)
        {

            Random rnd = new Random();
            int numeroAleatorio = rnd.Next(palabras.Length); 
          
            string input = null;
            Console.WriteLine("Introduce una palabra de 5 letras");
            Acciones(palabras[numeroAleatorio], input);
        }
    

        //Programa donde hace las accions principales de juegos 
        public void Acciones(string palabrasecreta, string input)
        {
           // Console.WriteLine(palabrasecreta);
            int intentosrest = 6 - intentos;
            Console.WriteLine("Quedan "+intentosrest+ " intentos");
            if (intentos<=5)
            {
                input = Console.ReadLine();

                if (input.Length >= 5)
                {
                    if (input == palabrasecreta)
                    {
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.WriteLine(palabrasecreta);
                        Console.ResetColor();
                        Console.WriteLine("Victoria");

                        Console.WriteLine("Felicidades, has acertado la palabra " + palabrasecreta + ".");
                        GuardarPartidaRanking();
                        Console.ReadLine();
                    }
                    else
                    {

                        Pistas(palabrasecreta, input);

                        Console.ResetColor();
                        intentos++;

                        Console.WriteLine("vuelve a intentar");
                        Acciones(palabrasecreta, input);

                    }
                }
                else if (input == "END")
                {
                    PartidaFinalizada(palabrasecreta);

                }

                else
                {
                    PalabraCorta(palabrasecreta, input);
                }
            }
            else if (intentos==6)
            {
                PartidaFinalizada(palabrasecreta);
            }

        }

        //Programa que indica hace el final de la partida cuando el jugador la ha cancelado (END) o la ha perdido
        public void PartidaFinalizada(string palabrasecreta)
        {
            Console.ResetColor();

            Console.WriteLine("Partida Finalizada");
            Console.WriteLine("La palabra secreta era [" + palabrasecreta + "]");
            Console.ReadLine();

            Program p = new Program();
            p.Inici();
        }

        //Prorama donde se pintan las letras para dar las pistas al jugador
        public void Pistas(string palabrasecreta, string input)
        {
            for (int i = 0; i < palabrasecreta.Length; i++)
            {
                if (palabrasecreta[i] == input[i])
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.Write(input[i]);
                }
                else
                {
                    if (palabrasecreta.Contains(input[i]))
                    {

                        Console.BackgroundColor = ConsoleColor.Yellow;
                        Console.Write(input[i]);
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.Write(input[i]);
                    }
                }
                if (i == palabrasecreta.Length - 1)
                {
                    Console.WriteLine("");
                }
            }
        }

        // Programa donde si el jugador introduce una palabra con menos de 5 letras dejara que el jugador vuelva a intentarlo sin sumarle intentos 
        public void PalabraCorta(string palabrasecreta, string input)
        {
            Console.ResetColor();
            
            Console.WriteLine("La palabra introducida es demasiado corta, vuelve a intentarlo");
            Acciones(palabrasecreta, input);

        }

        //Programa que pide el nombre del jugador si ha ganado y lo escribe junto a sus puntos en el archivo de texto
 
        void GuardarPartidaRanking()
        {
            Console.WriteLine("Creando nueva partida...");
            Console.WriteLine("Introduce el nombre de jugador");
            playerName = Console.ReadLine();
            string filePath = @"..\..\..\Ranking.txt";
           

            using (StreamWriter writer = new StreamWriter(filePath,true))
            {
                writer.WriteLine("El Jugador: "+playerName+" ha conseguido ganar en "+ intentos + " intentos");
            }
            Console.WriteLine("Partida guardada exitosamente!");
            Console.WriteLine("Pulsa una tecla para continuar");
        }
        

    }
}
